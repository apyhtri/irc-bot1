import socket
import sqlite3
import time
import urllib
import random
import time
import datetime
import pyreadline

class DB(object):
   def __init__(self):
        self.db_path = 'lollipopguild.db'
        self.define_table = 'define'

        self.conn = sqlite3.connect(self.db_path)
        self.c = self.conn.cursor()

        sql = 'create table if not exists ' + self.define_table + ' (id integer primary key, key text, value text, epoch_timestamp text)'
        self.c.execute(sql)

        self.c.close()
        self.conn.close()
        self.conn = sqlite3.connect(self.db_path)
        self.c = self.conn.cursor()

   def dict_factory(cursor, row):
            d = {}
            for idx, col in enumerate(cursor.description):
                d[col[0]] = row[idx]
            return d
         
            self.conn.row_factory = dict_factory
            self.c = self.conn.cursor()

   def get_defines(self, key, random_one=False):
        query_tail =""
        if random_one:
           query_tail = " ORDER BY RANDOM () LIMIT 1"
        return_list = []
        for row in self.c.execute('SELECT * FROM ' + self.define_table + ' WHERE key=?' + query_tail, (key.decode('iso-8859-15'),)):
            return_list.append({"id":row['id'],"key":row['key'].encode('iso-8859-15'),"value":row['value'].encode('iso-8859-15'), "epoch_timestamp":row['epoch_timestamp']})

        return return_list

   def insert_define(self, key, value):
        epoch_timestamp = time.time()
        self.c.execute("INSERT INTO " + self.define_table + " VALUES (NULL,?,?,?)", (key.decode("iso-8859-15"),
            value.decode("iso-8859-15"),epoch_timestamp))
        self.conn.commit()

   def get_random_key(self):
        return_val = ""
        for row in self.c.execute('SELECT DISTINCT key FROM ' + self.define_table + ' ORDER BY RANDOM() LIMIT 1'):
            print(row)
            return_val = row['key']
        return int(return_val)
        

class Mybot(object):    

   def __init__(self):
      self.DB = DB()
      network = 'irc.suomi.net'
      port = 6667
      self.irc = socket.socket ( socket.AF_INET, socket.SOCK_STREAM )
      self.irc.connect ( ( network, port ) )
      
      self.irc.send ( 'NICK RaivoRaimo\r\n' )
      self.irc.send ( 'USER botty botty botty :Python IRC\r\n' )
      self.irc.send ( 'JOIN #lollipopguild\r\n' )
      
        
   def handle_message(self, connection, event):
        msg = event.arguments()[0]
        if event.target()[0] != "#":
            return
        if msg[0] != "!":
            return
        
        author = event.source().split("!")[0]
        target = event.target()
        command, key, value = Parser.parse(msg)
        output = []

        if command == "define":     
            output = self._handle_define(key, value)

        elif command == "get":
            output = self._handle_get(key)
   def _handle_define(self, key, value):
        if key is "" or value is "":
            return ["Key or value missing."]
        self.DB.insert_define(key,value)
        return ['"' + key.lower() + '" defined.']

   def _handle_get(self, key, random_one=False):
        output = []
        if key is "":
            return ["Key missing."]
        defines = self.DB.get_defines(key, random_one=random_one)
        count = 1
        for define in defines:
            def_str = key + "[" + str(count) + "]: " + define["value"]
            output.append(def_str)
            count = count + 1
        if count == 1:
            output.append("'" + key + "' not found.")
        return output

   def _handle_random(self):
        return self._handle_get(self.DB.get_random_key(), random_one=True)

   def getweather(self):
       from bs4 import BeautifulSoup
       html =urllib.urlopen( 'http://weather.willab.fi/weather.html')
       soup=BeautifulSoup(html)
       newdoc = soup.find('p', class_="tempnow")
       self.irc.send('PRIVMSG #lollipopguild :Current weather in Linnanmaa:' +" " +newdoc.text.encode('iso-8859-15')+ '\r\n')
       
      
   def quote(self):
       try:
          import pyreadline as readline
       except ImportError:
          import readline
         
       from random import randint
       rng = 0
       rng = random.randint(0, 26)
       filename = open("C:\Users\Aleksi\Desktop\irkkibotti\irc-bot1\quotes.txt")
       line = filename.readlines()
      
       self.irc.send('PRIVMSG #lollipopguild :' +" " + str(line [rng])+'\r\n')
       filename.close()
   def date(self):
       now = datetime.datetime.now()
       self.irc.send('PRIVMSG #lollipopguild :Current date is:'+" "+ str(now) [:10]+ '\r\n')
                    
   def randomnumber(self):
       from random import randint

       rgn = 0
       rgn = random.randint(0, 1000)
       self.irc.send('PRIVMSG #lollipopguild :Here is your randomly generated number:'+ " " + str(rgn)+ '\r\n')
      
   def fibo(self):
       f1 = 0
       f2 = 1
       f3 = 1
       print f1
       print f2
       while f3 < 55:
          f1 = f2
          f2 = f3
          f3 = f1 + f2
         
          print f3 
   
   @classmethod
   def _output_lines_to_irc(cls, rows, connection, target):
        for row in rows:
            connection.privmsg(target, row)

   def processForever(self):
     while True:
       data = self.irc.recv (1024)
       #crude as hell but it works
       if data.find ( 'PING' ) != -1:
          self.irc.send ( 'PONG ' + data.split() [ 1 ] + '\r\n' )
       if data.find ( '!raimo quit' ) != -1:
          self.irc.send ( 'PRIVMSG #lollipopguild :Fine, if you do not want me\r\n' )
          self.irc.send ( 'QUIT\r\n' )
       if data.find('!randomnumber')!=-1:
          self.randomnumber()
       if data.find('!date')!=-1:
          self.date()
       if data.find('!quote')!=-1:
          self.quote()
       if data.find ( 'hi raimo' ) != -1:
          self.irc.send ( 'PRIVMSG #lollipopguild ' + data.split('!') [0] + ': I already said hi...\r\n'  )
       if data.find ( 'Hello raimo' ) != -1:
          self.irc.send ( 'PRIVMSG #lollipopguild ' + data.split('!') [0] +': I already said hi...\r\n' )
       if data.find ( 'KICK' ) != -1:
          self.irc.send ( 'JOIN #lollipopguild\r\n' )
       if data.find ( 'Cheese' ) != -1:
          self.irc.send ( 'PRIVMSG #lollipopguild ' + data.split('!') [0] + ': WHERE!!!!!!\r\n' )
       if data.find ( 'slaps Raimoraivo' ) != -1:
          self.irc.send ( 'PRIVMSG #lollipopguild ' + data.split('!') [0] + ': Why did you do that? :(\r\n' )
       if data.find('ohayoo') != -1:
          self.irc.send (' PRIVMSG #lollipopguild ' + data.split('!') [0] + ': Ohayoo gozaimasu!\r\n')
       if data.find ('Tu tu ru') != -1:
          self.irc.send (' PRIVMSG #lollipopguild ' + data.split('!') [0] +  ': Tu tu ru~\r\n')
       if data.find('!party')!= -1:
          self.irc.send('PRIVMSG #lollipopguild : OOOOOOOH YEEEEEEEEEAH *RaivoRaimo puts on some good music*\r\n ')
       if data.find ('!gangnam style')!=-1:
          self.irc.send('PRIVMSG #lollipopguild : OPPA GANGNAM STYLE! http://www.youtube.com/watch?v=9bZkp7q19f0\r\n')
       if data.find ('!halp') != -1:
          self.irc.send ('PRIVMSG #lollipopguild :every command starts with "!"\r\n  ')
          self.irc.send ('PRIVMSG #lollipopguild :define lets you place definitions in to the database.\r\n')
          self.irc.send ('PRIVMSG #lollipopguild :get lets you pull information out of the database.\r\n')
          self.irc.send ('PRIVMSG #lollipopguild :party command will cause Raimo to go nuts.\r\n')
          self.irc.send ('PRIVMSG #lollipopguild :weather command gives you the current weather in linnnanmaa.\r\n')
          self.irc.send ('PRIVMSG #lollipopguild :date gives you the current date\r\n')
          self.irc.send ('PRIVMSG #lollipopguild :quote will give you a random famous people quote.\r\n')
          self.irc.send ('PRIVMSG #lollipopguild :intro will introduce Raimo\r\n')
       if data.find('!random') != -1:
          output = self._handle_random()
          for define in output:
             
             self.irc.send('PRIVMSG #lollipopguild :' + " " +define+ '\r\n')
       
          
       if data.find('!weather') !=-1:
          self.getweather()
       if data.find('!fibonacci') !=-1:
          self.fibo()
       if data.find ('!intro') !=-1:
          self.irc.send( ' PRIVMSG #lollipopguild :I am RaivoRaimo, bot extraordinaire and a gentleman! I can do tricks when my lazy ass creator implements them. I also use a define-database for your convenience.\r\nYou can also type !halp for more information.')
       if data.find ('!define') != -1:
         
          #insert into db data
          command, key, value = Parser.parse(data.split (" ", 2) [2][:-2])
          self.DB.insert_define(key, value)
          
          self.irc.send ('PRIVMSG #lollipopguild :' + " " +key+ ' defined \r\n')
          
       if data.find('!get') != -1:
          command, key, value = Parser.parse(data.split (" ", 2) [2] [:-2])
          #get data from db
          output = self._handle_get(key)
          for define in output:
             
             
             
             out_str = ('PRIVMSG #lollipopguild :' + " " +define+ '\r\n')
             self.irc.send (out_str)
             
          
       
        
#parsing the data for database
class Parser(object):
    @classmethod
    def parse(cls, row):

        
        split_list = row.split(" ", 3)
        command = split_list[1][:] # skip the first character
        command = command.lower()
        key = ""
        if len(split_list) > 2:
            key = split_list[2] [:]
            key = key.lower()
        value = ""
        if len(split_list) > 3:
            value = split_list[3]
        return command, key, value

#program starts actually from here
ib = Mybot()
ib.processForever()
